library(dplyr)
library(igraph)

# Function
properties <- function(g){
	cat("vcount : ",vcount(g),"\n")
	cat("ecount : ",ecount(g),"\n")
	cat("graph.density : ",graph.density(g),"\n")
	cat("diameter : ",diameter(g),"\n")
	# cat("degree : ",degree(g),"\n")
	cat("degree.distribution : ",degree.distribution(g),"\n")
	cat("transitivity : ",transitivity(g),"\n")
	cat("shortest.paths : ",shortest.paths(g),"\n")
	cat("betweenness :",betweenness(g),"\n")
	cat("closeness : ",closeness(g),"\n")
	cat("is.connected : ",is.connected(g),"\n")
	print(clusters(g))
	# cat("neighbors : ",neighbors(g),"\n")
}

mod_R <- function(graph, C, B, S){
  bin <- length(E(graph)[B%--%B])
  bout <- length(E(graph)[B%--%S])
  return (bin/(bin+bout))
}

mod_M <- function(graph, C, B, S){
 D <- union(B,C)
 din <- length(E(graph)[D%--%D])
 dout <- length(E(graph)[B%--%S])
 return (din/dout)
}

mod_L <- function(graph, C, B, S){
  D <- union(C,B)
  lin <- 0
  lout <- 0
  for (node in D)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, D)
    lin <- lin+length(inter)/length(D)
  }
  for (node in B)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, S)
    lout <- lout+length(inter)/length(B)
  }
  
  return (lin/lout)
}


in_c <- function(g, node, D){
  voisins <- neighbors(g, node)
  for (i in voisins){
    if (length(D)>1)
    {
      if (!(i %in% D))
      {
        return (FALSE)
      }
    }
  }
  return (TRUE)
}

update <- function(g, node_to_move, C, B, S){
  D <- union(B,C)
  S <- S[S!=node_to_move]
  if (in_c(g, node_to_move, D) == TRUE){ # A RETESTER avec S ou D
    C <- union(C, node_to_move)
  }
  else{
    B <- union(B, node_to_move)
    s_update <- setdiff(neighbors(g, node_to_move), union(D,S))
    if (length(s_update)>0)
    {
      S <- V(g)[V(g)$id %in% union(S$id, s_update)]
    }
    for (node in B)
    {
      if (in_c(g, node, D)==TRUE){
        B <- B[B!=node]
        C <- union(C, node)
      }
    }
  }
  return (list(C=C,B=B,S=S))
}

qualite1 <- function(g, node, C, B, S, modularite){
  tmp <- update(g, node, C, B, S)
  C <- tmp$C
  B <- tmp$B
  S <- tmp$S
  return (modularite(g, C, B, S))
  
}

qualite2 <- function(g, node, C, B, S, modularite){  # here modularite is a list
  tmp <- update(g, node, C, B, S)
  C <- tmp$C
  B <- tmp$B
  S <- tmp$S
  mR <- modularite[[1]](g,C,B,S)
  mM <- modularite[[2]](g,C,B,S)
  mL <- modularite[[3]](g,C,B,S)
  cat('mR :',mR,'\n')
  cat('mM :',mM,'\n')
  cat('mL :',mL,'\n')
  cat('\n')
  res  = mean(c(mR,mM,mL))
  print(res)
  return (res)
  
}

qualite3 <- function(g, node, C, B, S, modularite){
  tmp <- update(g, node, C, B, S)
  C <- tmp$C
  B <- tmp$B
  S <- tmp$S
  res <- c()
  i=0
  for(m in modularite){
    l <- c()
    for(n in S){
      tmp2 <- update(g,n,C,B,S)
      C <- tmp2$C
      B <- tmp2$B
      S <- tmp2$S
      l <- c(l,m(g,C,B,S))  
    }
    minQs <- min(l)
    maxQs <- max(l)
    mod <- m(g,C,B,S)
    if(maxQs != minQs && !is.nan(minQs) && !is.nan(maxQs)){
      print('ALERT')
      QiS <- (mod - minQs)/(maxQs-minQs)
    }else{
      QiS <- 1
    }
    res <- c(res,QiS)
  }
  return(mean(res))
}


local_com <- function(g, n0, modularite){
  # initialisation
  C <- c()
  B <- c(n0)
  S <- neighbors(g, n0)
  Q0 <- 0
  Q1 <- 100000000
  
  # récurrence
  while ((Q1>Q0) && (length(S)>0))
  {
    l <- c()
    for (node in S){
      mod <- qualite1(g, node, C, B, S, modularite)
      l <- c(l, mod)
    }
    Q1 <- max(l)
    if (Q1 > Q0){
      node_to_move <- S[which.max(l)]
      tmp <- update(g, node_to_move, C, B, S)
      C <- tmp$C
      B <- tmp$B
      S <- tmp$S
      Q0 <- Q1
      Q1<-Q1+1
    }
  }
  return (union(B,C))
}


recomandation <- function(node,com_loc,g){
  voisins <- neighbors(g,node)
  candidats <- com_loc[!(com_loc %in% voisins)]
  candidats <- candidats[candidats!=node]
  res=data.frame()
  for(c in candidats){
    v <- neighbors(wiki,c)
    score = length(intersect(v,voisins))
    res <- rbind(res,data.frame(node=c,score=score))
  }
  return(subset(res,score==max(score))$node)
}


wiki=read.graph("wikipedia.gml", format="gml")
# l = local_com(wiki, 101, mod_L)
# print(l)
# r=recomandation(101,l,wiki)
# print(r)

for(node in V(wiki)[seq(1, 5000, 53)]){
  #cat("COM LOC : ",local_com(wiki, node, mod_L),"\n")
  #cat("neighbors : ",neighbors(wiki,node),"\n \n")
  l = local_com(wiki, node, mod_L)
  n = c(node,neighbors(wiki,node))
  res = all(l %in% n)
  cat("NODE : ",node," - ",res,"\n")
  if(res==FALSE)
    break
}

