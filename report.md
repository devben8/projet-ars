# Projet ARS

Aymeric Duchein, Rayan Hadjeres, Benoit Pimpaud, Khalil Soare

**Sujet 6 : Recommendation de liens entre pages Wikipédia**

**Enoncé** : Une approche simple de recommendation de liens entre page wikipédia
consiste à calculer la communauté égo-centré d’une page puis recommander
de la lier la page la plus proche dans la communauté et qui n’est pas
connectée à la page cible. Les résultats de la recommendation dépende de la
fonction qualité à utiliser pour le calcul de la communauté locale. Proposer
une version multi-critère de l’algorithme classique de calcule de communauté
locale. Valider l’approche sur le jeu de données Wikipedia disponible sur le
site du cours.

## Données

Les données utilisées représentent le graphe de plusieurs articles issues de Wikipédia. Le fichier `gml` est disponible à cette adresse : http://lipn.fr/~kanawati/ars/ .

## Calcul d'une communauté

Afin de calculer la communauté locale d'une page (ie. d'un noeud) nous utilisons la stratégie d'optimisation gloutonne avec les fonctions de modularité locales ci-dessous :

```
mod_R <- function(graph, C, B, S){
  bin <- length(E(graph)[B%--%B])
  bout <- length(E(graph)[B%--%S])
  return (bin/(bin+bout))
}

mod_M <- function(graph, C, B, S){
  D <- union(B,C)
  din <- length(E(graph)[D%--%D])
  dout <- length(E(graph)[B%--%S])
  return (din/dout)
}

mod_L <- function(graph, C, B, S){
  D <- union(C,B)
  lin <- 0
  lout <- 0
  for (node in D)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, D)
    lin <- lin+length(inter)/length(D)
  }
  for (node in B)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, S)
    lout <- lout+length(inter)/length(B)
  }
  
  return (lin/lout)
}
```

Pour adapter l'algorithme classique avec une version multi-critère nous allons utiliser une fonction de qualité, fonction des modularités présentés ci-dessus. 
La modularité est une mesure pour la qualité d'un partitionnement des nœuds d'un graphe, ou réseau, en communautés. Elle est décrite comme la proportion des arêtes incidentes sur une classe donnée moins la valeur qu'aurait été cette même proportion si les arêtes étaient disposées au hasard entre les noeuds du graphe.

Afin d'obtenir une fonction de qualité regroupant plusieurs fonctions de modularités, nous nous somme appuyé sur une méthode de combinaison, développée ci-dessous :

![combine_rank_math](https://i.imgur.com/KpoHCpC.png)

```
qualite3 <- function(g, node, C, B, S, modularite){
  tmp <- update(g, node, C, B, S)
  C <- tmp$C
  B <- tmp$B
  S <- tmp$S
  res <- c()
  i=0
  for(m in modularite){
    l <- c()
    for(n in S){
      tmp2 <- update(g,n,C,B,S)
      C <- tmp2$C
      B <- tmp2$B
      S <- tmp2$S
      l <- c(l,m(g,C,B,S))  
    }
    minQs <- min(l)
    maxQs <- max(l)
    mod <- m(g,C,B,S)
    if(maxQs != minQs && !is.nan(minQs) && !is.nan(maxQs)){
      print('ALERT')
      QiS <- (mod - minQs)/(maxQs-minQs)
    }else{
      QiS <- 1
    }
    res <- c(res,QiS)
  }
  return(mean(res))
}
```

En annexes, vous trouverez le développement de la fonction de détection de communauté locale (reprise du tp3, disponible à cette adresse : http://lipn.fr/~kanawati/ars/tp3-ARS-cor.pdf)


## Recommandation

Afin de ne pas avoir les liens directement connectés à la page cible, nous enlevons les voisins de la communauté locale calculée.
Enfin, pour avoir un seul lien a recommander, nous prenons le noeud possédant le moins de voisins.

```
critere <-function(l,wiki){
  for (i in seq(1:length(l))){
    if (min(degree(wiki,l))==degree(wiki,l[i])){
      return(l[i])
      break
    }
  }
}

recomandation <- function(node,com_loc,g){
  voisins <- neighbors(g,node)  # les voisins du noeud cible
  com_loc_without_neighbors=com_loc[!(com_loc %in% voisins)]  # tous les noeuds appartennant à la communauté locale mais n'étant pas voisin du noeud cible
  final_node = critere(com_loc_without_neighbors,g) # prend le noeud qui a le plus de liens (à partir de com_loc_without_neighbors)
  return(list(com_loc_without_neighbors,final_node))
}
```

