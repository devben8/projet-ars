# core2.r

library(dplyr)
library(igraph)

mod_R <- function(graph, C, B, S){
  bin <- length(E(graph)[B%--%B])
  bout <- length(E(graph)[B%--%S])
  return (bin/(bin+bout))
}

mod_M <- function(graph, C, B, S){
 D <- union(B,C)
 din <- length(E(graph)[D%--%D])
 dout <- length(E(graph)[B%--%S])
 return (din/dout)
}

mod_L <- function(graph, C, B, S){
  D <- union(C,B)
  lin <- 0
  lout <- 0
  for (node in D)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, D)
    lin <- lin+length(inter)/length(D)
  }
  for (node in B)
  {
    gamma_node <- neighbors(graph, node)
    inter <- intersect(gamma_node, S)
    lout <- lout+length(inter)/length(B)
  }
  
  return (lin/lout)
}

compute_quality <- function(n,g,C,B,S,mod){
  res <- update(n,g,C,B,S)
  C <- res$C
  B <- res$B
  S <- res$S
  return(mod(g,C,B,S))
}

update<- function(n,g,C,B,S){
	S<-S[S!=n]
	D<-union(C,B)
	if(all(neighbors(g,n) %in% D)){
		C<-union(c,n)
	}else{
		B<-union(B,n)
		new_s=setdiff(neighbors(g,n),union(D,S))
		if(length(new_s)>0){
			S<-union(S,new_s)
		}
		for(b in B){
			if(all(neighbors(g,b) %in% D)){
				B<-B[B!=b]
				C<-union(C,b)
			}
		}	
	}
	return(list(C=C,B=B,S=S))
}

local_com <- function(target,g,mod){
  if(is.igraph(g) && target %in% V(g)){
    C <- c()
    B <- c(target)
    S <- c(V(g)[neighbors(g,target)]$id)
    Q <- 0
    new_Q <- 0
    while ((length(S)>0) && (new_Q >= Q)){
      QS <- sapply(S,compute_quality,g,C,B,S,mod)
      new_Q <- max(QS)
      if(new_Q >= Q){
        s_node <- S[which.max(QS)]
        res <- update(s_node,g,C,B,S)
        C <- res$C
        B <- res$B
        S <- res$S
        Q <- new_Q
      }
    }
    return(union(C,B))
  }else{
    stop("invalid arguments")
  }
}

wiki=read.graph("wikipedia.gml", format="gml")
# sub_wiki=subgraph.edges(wiki, E(wiki)[1:100], delete.vertices = TRUE)
# l = local_com(wiki, 12, mod_L)
l = local_com(10,wiki, mod_M)
print(l)