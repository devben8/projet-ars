# Mini-projet : ARS
## Enoncé
P6 : Recommendation de liens entre pages Wikipédia
Une approche simple de recommendation de liens entre page wikipédia
consiste à calculer la communauté égo-centré d’une page puis recommander
de la lier la page la plus proche dans la communauté et qui n’est pas
connectée à la page cible. Les résultats de la recommendation dépende de la
fonction qualité à utiliser pour le calcul de la communauté locale. Proposer
une version multi-critère de l’algorithme classique de calcule de communauté
locale. Valider l’approche sur le jeu de données Wikipedia disponible sur le
site du cours.